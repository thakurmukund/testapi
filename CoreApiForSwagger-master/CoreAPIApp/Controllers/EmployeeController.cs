﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Logging;
using System.IO;

namespace CoreAPIApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
		[HttpGet]
		public ActionResult EmpXML()
		{
			XmlDocument doc = new XmlDocument();
			doc.Load(@"App_Data\Employee.xml");
			
			HttpResponseMessage response = new HttpResponseMessage();

			//Json json = JsonConvert.SerializeXmlNode(doc);
	//		response.Content = new StringContent(json, Encoding.UTF8, "application/json");

			return Content(JsonConvert.SerializeXmlNode(doc));
		}
	}
}